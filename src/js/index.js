let slideIndex = 1;
let slideTimer;

showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  let i;
  let slides = document.getElementsByClassName("mySlides");
  let dots = document.getElementsByClassName("dot");

  if (n > slides.length) { slideIndex = 1 }
  if (n < 1) { slideIndex = slides.length }

  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";
  }

  for (i = 0; i < dots.length; i++) {
    dots[i].className = dots[i].className.replace(" active", "");
  }

  slides[slideIndex - 1].style.display = "block";
  dots[slideIndex - 1].className += " active";

  // Reinicia o timer para a transição automática
  clearTimeout(slideTimer);
  slideTimer = setTimeout(() => {
    plusSlides(1); // Avança para o próximo slide automaticamente
  }, 3000); // Intervalo de 3 segundos (3000 milissegundos)
}

// Inicia o timer de transição automática ao carregar a página
slideTimer = setTimeout(() => {
  plusSlides(1); // Avança para o próximo slide automaticamente
}, 3000); // Intervalo de 3 segundos (3000 milissegundos)