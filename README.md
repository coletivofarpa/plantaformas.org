# Bem-vindes à biblioteca de componentes modulares para o Decidim da [Plantaformas.org](https://plantaformas.org)
Vamos construir uma sociedade mais aberta, transparente e colaborativa. <br>
Cadastre-se nas instancias do Decidim, participe e decida.

## Como conhecer esta biblioteca de componentes:
Basta clonar este repositório para a sua máquina e visualizar o arquivo index.html no navegador web:
```
git clone https://gitlab.com/coletivofarpa/plantaformas.org.git
```